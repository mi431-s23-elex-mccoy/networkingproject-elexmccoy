using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Component.Animating;

public class Animating : MonoBehaviour
{
    private Animator _animator;
    private NetworkAnimator _networkAnimator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _networkAnimator = GetComponent<NetworkAnimator>();
    }

    public void SetMoving(bool value)
    {
        _animator.SetBool("Moving", value);
    }

    public void Jump()
    {
        _networkAnimator.SetTrigger("Jump");
    }
}
